package fi.jamk.l3329.stockpile;

import android.app.DialogFragment;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        ShoplistDialogFragment.ShopListDialogListener,
        ItemAdapter.OnItemCheckListener

{


    private static final int REQUEST_CODE_RESOLUTION = 1;
    private GoogleApiClient mGoogleApiClient;
    private static final String TAG = "MainActivity";

    private LinearLayoutManager mLayoutManager;
    private RecyclerView mRecyclerView;
    private List<Item> items;

//    private boolean fileOperation = false;

    private DatabaseOpenHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initFab();

        //get database instance
        db = new DatabaseOpenHelper(this);

        //get data with using own made queryData method
        items = db.getAllItems();

//        items = new ArrayList<>();

        mRecyclerView = (RecyclerView) findViewById(R.id.recycle_view);
        mRecyclerView.setAdapter(new ItemAdapter(items, this, this));
        registerForContextMenu(mRecyclerView);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        //Initialize Google Drive API Client
        connectAPIClient();
    }


    public void initFab() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShoplistDialogFragment dialogFragment = new ShoplistDialogFragment();
                dialogFragment.show(getFragmentManager(), "newShopList");

            }
        });
    }


    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String name, float price, float amount, String currency, String unit) {
        Item item = new Item.ItemBuilder()
                .name(name)
                .price(price)
                .amount(amount)
                .currency(currency)
                .units(unit)
                .build();

        items.add(item);
        db.addItem(item);

        mRecyclerView.setAdapter(new ItemAdapter(items, this, this));
        Snackbar.make(findViewById(android.R.id.content), "Successfully created " + name, Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        Snackbar.make(findViewById(android.R.id.content), "Canceled", Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show();
    }


    private void connectAPIClient() {
        if (mGoogleApiClient == null) {
            //create the API client and bind it to an intance variable
            //We use "this" instance as the callback for connection and connection failures
            //since no account name is passed, the user is prompted to choose

            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Drive.API)
                    .addScope(Drive.SCOPE_APPFOLDER)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onPause() {
        if (mGoogleApiClient != null) {


            mGoogleApiClient.disconnect();
        }
        super.onPause();
    }

//    /**
//     * This is Result result handler of Drive contents.
//     * this callback method call CreateFileOnGoogleDrive() method.
//     */
//    final ResultCallback<DriveApi.DriveContentsResult> driveContentsCallback =
//            new ResultCallback<DriveApi.DriveContentsResult>() {
//        @Override
//        public void onResult(DriveApi.DriveContentsResult result) {
//
//            if (result.getStatus().isSuccess()) {
//                if (fileOperation == true){
//
//                    CreateFileOnGoogleDrive(result);
//
//                }
//            }
//        }
//    };
//
//
//    /**
//     * Create a file in root folder using MetadataChangeSet object.
//     * @param result
//     */
//    public void CreateFileOnGoogleDrive(DriveApi.DriveContentsResult result){
//
//        final DriveContents driveContents = result.getDriveContents();
//
//
//        // Perform I/O off the UI thread.
//        new Thread() {
//            @Override
//            public void run() {
//                // write content to DriveContents
//                OutputStream outputStream = driveContents.getOutputStream();
//                Writer writer = new OutputStreamWriter(outputStream);
//                try {
//                    writer.write("Hello abhay!");
//                    writer.close();
//
//                } catch (IOException e) {
//                    Log.e(TAG, e.getMessage());
//                }
//
//                MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
//                        .setTitle("abhaytest3")
//                    .setMimeType("text/plain")
//                    .setStarred(true).build();
//
//                // create a file in root folder
//                Drive.DriveApi.getRootFolder(mGoogleApiClient)
//                        .createFile(mGoogleApiClient, changeSet, driveContents)
//                .       setResultCallback(fileCallback);
//            }
//        }.start();
//    }
//
//    /**
//     * Handle result of Created file
//     */
//    final private ResultCallback<DriveFolder.DriveFileResult> fileCallback = new
//    ResultCallback<DriveFolder.DriveFileResult>() {
//        @Override
//        public void onResult(DriveFolder.DriveFileResult result) {
//            if (result.getStatus().isSuccess()) {
//
//                Toast.makeText(getApplicationContext(), "file created:"+" "+
//                        result.getDriveFile().getDriveId(), Toast.LENGTH_LONG).show();
//
//            }
//
//            return;
//
//        }
//    };

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null) {
//            fileOperation = true;
//            // create new contents resource
//            Drive.DriveApi.newDriveContents(mGoogleApiClient)
//                    .setResultCallback(driveContentsCallback);
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onDestroy();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
//        Log.i(TAG, "GoogleAPIClient successfully connected.");
        Toast.makeText(this, "In onConnected activity", Toast.LENGTH_SHORT).show();

        //Databases are stored in the /data/data/<package name>/databases folder.
        // Testing to see if database is indeed uploaded to google drive app folder
//        String db_name = db.getDatabaseName();
//        String currentDBPath = "/data/data" + "fi.jamk.l3329.stockpile" + "/databases/" + db_name;

//        saveToDrive(
//                Drive.DriveApi.getAppFolder(mGoogleApiClient),
//                db_name + ".db",
//                "application/x-sqlite3",
//                new java.io.File(currentDBPath)
//        );
//        saveToDrive(
//                Drive.DriveApi.getAppFolder(getGoogleApiClient()),
//                "MyDbFile.db",
//                "application/x-sqlite3",
//                new java.io.File("\...\...\...\MyDbFile.db")
//        );
    }


    DriveId mDriveId;



    /******************************************************************
     * create file in GOODrive
     * @param pFldr parent's ID
     * @param titl  file name
     * @param mime  file mime type  (application/x-sqlite3)
     * @param file  file (with content) to create
     */
    void saveToDrive(final DriveFolder pFldr, final String titl, final String mime, final java.io.File file) {
        if (mGoogleApiClient != null && pFldr != null && titl != null && mime != null && file != null)
            try {
                // create content from file
                Drive.DriveApi
                        .newDriveContents(mGoogleApiClient)
                        .setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>() {
                            @Override
                            public void onResult(DriveApi.DriveContentsResult result) {
                                DriveContents cont = result != null && result.getStatus().isSuccess() ? result.getDriveContents() : null;

                                // write file to content, chunk by chunk
                                if (cont != null) try {
                                    OutputStream output = cont.getOutputStream();
                                    if (output != null) {
                                        try {
                                            InputStream is = new FileInputStream(file);
                                            byte[] buf = new byte[4096];
                                            int c;
                                            while ((c = is.read(buf, 0, buf.length)) > 0) {
                                                output.write(buf, 0, c);
                                                output.flush();
                                            }
                                        } finally {
                                            output.close();
                                        }
                                    }

                                    // content's COOL, create metadata
                                    MetadataChangeSet meta = new MetadataChangeSet.Builder().setTitle(titl)
                                                                                            .setMimeType(mime)
                                                                                            .build();

                                    // now create file on GooDrive
                                    pFldr.createFile(mGoogleApiClient, meta, cont)
                                            .setResultCallback(new ResultCallback<DriveFolder.DriveFileResult>() {
                                                @Override
                                                public void onResult(DriveFolder.DriveFileResult driveFileResult) {
                                                    if (driveFileResult != null && driveFileResult.getStatus().isSuccess()) {
                                                        DriveFile dFil = driveFileResult != null && driveFileResult.getStatus().isSuccess() ?
                                                                driveFileResult.getDriveFile() : null;
                                                        if (dFil != null) {
                                                            // BINGO , file uploaded
                                                            dFil.getMetadata(mGoogleApiClient).setResultCallback(new ResultCallback<DriveResource.MetadataResult>() {
                                                                @Override
                                                                public void onResult(DriveResource.MetadataResult metadataResult) {
                                                                    if (metadataResult != null && metadataResult.getStatus().isSuccess()) {
                                                                        DriveId mDriveId = metadataResult.getMetadata().getDriveId();
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    } else { /* report error */ }
                                                }
                                            });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    /*******************************************************************
     * get file contents
     */
    void readFromGooDrive() {
        byte[] buf = null;
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            try {
                DriveFile df = Drive.DriveApi.getFile(mGoogleApiClient, mDriveId);
                df.open(mGoogleApiClient, DriveFile.MODE_READ_ONLY, null)
                        .setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>() {
                            @Override
                            public void onResult(DriveApi.DriveContentsResult result) {

                                if ((result != null) && result.getStatus().isSuccess()) {
                                    DriveContents cont = result.getDriveContents();
                                    // DUMP cont.getInputStream() to your DB file
                                    cont.discard(mGoogleApiClient);    // or cont.commit();  they are equiv if READONLY
                                }

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "GoogleAPIClient connection suspended with code: " + i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Log.e(TAG, "GoogleAPIClient connection failed with result: " + result);
        Log.i(TAG, "Attempt to resolve connection problems.");
        //this happens when the app is not yet authorized


        if (!result.hasResolution()) {
            //show the localized error dialog
            GoogleApiAvailability.getInstance()
                    .getErrorDialog(this, result.getErrorCode(), 0)
                    .show();
            return;
        }

        /*called typically when the app is not yet authorized,
        authorization dialog is displayed to the user*/
        try {
            result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);
        } catch (IntentSender.SendIntentException e) {
            Log.e(TAG, "Exception while starting resolution activity with message: " + e.getMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemCheck(int position) {
        items.get(position).setBought(true);
        db.updateItem(items.get(position));
    }

    @Override
    public void onItemUncheck(int position) {
        items.get(position).setBought(false);
        db.updateItem(items.get(position));
    }
}
