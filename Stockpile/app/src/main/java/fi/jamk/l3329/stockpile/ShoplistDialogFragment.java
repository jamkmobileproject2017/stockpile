package fi.jamk.l3329.stockpile;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

/**
 * Created by peter on 4.11.2017.
 */

public class ShoplistDialogFragment extends DialogFragment{



    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks. */
    public interface ShopListDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog, String shopListName, float price, float amount);
        public void onDialogNegativeClick(DialogFragment dialog);
    }

    ShopListDialogListener mListener;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();




        final View dialogView = inflater.inflate(R.layout.new_item_dialog, null);



        builder.setView(dialogView)
                .setTitle(R.string.dialog_title)
                .setPositiveButton(R.string.dialog_add, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //find a shoplist name


                        EditText name = (EditText) dialogView.findViewById(R.id.name_item);
                        EditText price1 = (EditText) dialogView.findViewById(R.id.price_item);
                        EditText amount1 = (EditText) dialogView.findViewById(R.id.amount_item);






                        String shopListName = name.getText().toString().isEmpty() ? "New Item": name.getText().toString();
                        float price = Float.parseFloat(price1.getText().toString().isEmpty() ? "0" : price1.getText().toString());
                        float amount = Float.parseFloat(amount1.getText().toString().isEmpty() ? "0" : amount1.getText().toString());
                        // Send the positive button event back to the host activity
                        mListener.onDialogPositiveClick(ShoplistDialogFragment.this,shopListName, price, amount);
                    }
                })
                .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onDialogNegativeClick(ShoplistDialogFragment.this);
                    }
                });

        return builder.create();
    }

    // Override the Fragment.onAttach() method to instantiate the TeamDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the ShopListDialogListener so we can send events to the host
            mListener = (ShopListDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString() + " must implement ShopListDialogListener");
        }
    }
}
